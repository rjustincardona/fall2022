import numpy as np
import matplotlib.pyplot as plt


# Creates the initial conditions
def init_model(L):
    model = np.random.randint(-L, L, L)
    return model / 2


# Finding the energy of a model
def energy(model, J):
    E = 0
    for i in range(model.shape[0] - 1):
        E += model[i] - model[i - 1]
    return 0.5 * J * E


# Function to get g
def g(model):
    L = model.shape[0]
    result = np.zeros(L)
    for j in range(L):
        for i in range(L):
            result[j] += np.power((model[(i + j) % L] - model[i]), 2)

    return result / L


# Updates the model once (periodic boundary conditions used)
def update(model, J, B):
    L = model.shape[0]

    # Make random change to the model
    index = np.random.randint(0, L - 1)
    change = np.random.choice([-1, 1])
    if np.abs(model[index] + change) > 0.5 * L:
        change *= -1

    new_model = np.copy(model)
    new_model[index] += change

    # Get the energy difference
    de = energy(np.take(new_model, [(index - 1) % L, index, (index + 1) % L]), J) - energy(np.take(model, [(index - 1) % L, index, (index + 1) % L]), J)

    if de < 0:
        return new_model
    else:
        p = np.exp(-B * de)
        if np.random.rand() < p:
            return new_model
        else:
            return model


# Updates the model several times
def evolve(model, iter, J, B):
    m_temp = model
    for i in range(iter):
        m_temp = update(m_temp, J, B)
    return m_temp    


# Create the model
L = 11
iter = int(1e8)
domain = np.linspace(0, L, L)

m = init_model(L)
m_0 = evolve(m, iter, 1, 0)
m_1 = evolve(m, iter, 1, 1)
m_5 = evolve(m, iter, 1, 5)
s_0 = np.var(m_0)
s_1 = np.var(m_1)
s_5 = np.var(m_5)
np.savetxt("sigmas.txt", [np.sqrt(s_0), np.sqrt(s_1), np.sqrt(s_5)])
np.savetxt("heights.txt", [m_0, m_1, m_5])

# Plot the required information
plt.plot(domain, m_5, label = r"$\beta J = 5$")
plt.plot(domain, m_0, label = r"$\beta J = 0$")
plt.plot(domain, m_1, label = r"$\beta J = 1$")
plt.xlabel(r"j")
plt.ylabel(r"$h_j$")
plt.legend()
plt.savefig("h.png")
plt.close()

plt.plot(domain, g(m_0), label = r"$\beta J = 0$")
plt.plot(domain, g(m_1), label = r"$\beta J = 1$")
plt.plot(domain, g(m_5), label = r"$\beta J = 5$")
plt.xlabel(r"j")
plt.ylabel(r"$g_j$")
plt.legend()
plt.savefig("g.png")
plt.close()
