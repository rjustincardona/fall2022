import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

# Sampling
def sample(dist):
    if dist == "normal":
        return np.sqrt(2) * sp.special.erfinv(2 * np.random.rand() - 1)
    if dist == "cauchy":
        return np.tan(np.pi * (np.random.rand() - 0.5))
    if dist == "fat":
        return 1 / ((np.random.rand() - 1) ** 2)

# Outputs a sample trajectory
def make_traj(dist, T):
    traj = np.zeros(T, dtype = float)
    for i in range(1, T):
        traj[i] =  sample(dist) + traj[i - 1]
    return traj

T = 10 ** 7
for dist in ["normal", "cauchy", "fat"]:
    # Saving trajectory to files
    # trajectory = make_traj(dist, T)
    # np.savetxt(dist + ".gz", trajectory)
    # print("Completed: " + dist)
    # Loading the trajectory from files
    trajectory = np.loadtxt(dist + ".gz") # obtained by uncommenting the above
    dom = np.linspace(0, trajectory.shape[0], trajectory.shape[0])

    # Making the plot 
    if dist == "normal":
        left, bottom, width, height = [0.6, 0.25, 0.27, 0.3]
    elif dist == "cauchy":
        left, bottom, width, height = [0.25, 0.2, 0.3, 0.3]
    else:
        left, bottom, width, height = [0.25, 0.4, 0.3, 0.3]
    fig, ax1 = plt.subplots()
    ax2 = fig.add_axes([left, bottom, width, height])
    ax1.plot(dom, trajectory, color='red')
    ax2.plot(dom[500000: 510000], trajectory[500000: 510000], color='green')
    ax1.set_xlabel("Time steps")
    ax1.set_ylabel("Position")
    ax2.set_xlabel("Time steps")
    ax2.set_ylabel("Position")
    plt.savefig(dist + ".png")
    plt.close()
