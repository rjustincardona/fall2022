import numpy as np
import matplotlib.pyplot as plt

# Creating the system
p = 0.55
N = 1000
grid = np.zeros((N, N))
grid[0] = np.ones(N)
Y = np.argwhere(grid == 1)
x = np.empty(0)
Z = np.empty(0)

# Nearest Neighbours
def neighbours(idx):
    up = [idx[0], (idx[1] - 1) % N]
    down = [idx[0], (idx[1] + 1) % N]
    left = [(idx[0] - 1) % N, idx[1]]
    right = [(idx[0] + 1) % N, idx[1]]

    if idx[0] == 0:
        return np.array([up, down, right])
    elif idx[0] == N - 1:
        return np.array([up, down, left])
    return np.array([up, down, left, right])

# Getting <x> and <Z>
def get_x():
    numerator = 0
    Z = np.argwhere(grid == 2)
    return np.sum(Z.T[0]) / Z.shape[0], Z.shape

# While there are still infected people, infect people
while Y.shape[0] != 0:
    # Get all the neighbours of the infected, and immunize the infected
    n = np.empty((0, 2), dtype=int)
    for i in range(Y.shape[0]):
        idx = Y[0]
        Y = np.delete(Y, 0, 0)
        n = np.concatenate((n, neighbours(idx)), 0)
        grid[idx[0], idx[1]] = 2

    # Determine which of the neighbours get infected, infect them if needed
    for idx in n:
        if grid[idx[0], idx[1]] != 2:
            if np.random.rand() <= p:
                grid[idx[0], idx[1]] = 2
                Y = np.concatenate((Y, [idx]), 0)
    
    info_x, info_Z = get_x()
    x, Z = np.concatenate((x, info_x.reshape(1,)), 0), np.concatenate((Z, info_x.reshape(1,)), 0)

np.savetxt("grid_" + str(p) + ".gz", grid)
np.savetxt("x_" + str(p) + ".gz", x)
np.savetxt("Z_" + str(p) + ".gz", Z)
