import numpy as np
import matplotlib.pyplot as plt


p = 0.55

grid = np.loadtxt("grid_" + str(p) + ".gz")
x = np.loadtxt("x_" + str(p) + ".gz")
Z = np.loadtxt("Z_" + str(p) + ".gz")

# Due to the algorithm used here, the simulation always stops at equilibrium so there are no infected people at the end and 2 colors are needed
plt.imshow(grid, cmap='Greys')
plt.title("p = " + str(p))
plt.savefig("grid_" + str(p) + ".png")
plt.close()

# Plotting <x> linearly
domain = np.linspace(1, x.shape[0] + 1, x.shape[0])
plt.plot(domain, x)
plt.xlabel("time")
plt.ylabel(r"$\langle x\rangle_t$ for p = " + str(p))
plt.savefig("x_lin_" + str(p) + ".png")
plt.close()

# Plotting <Z>
plt.xlabel("time")
plt.ylabel(r"$\langle Z\rangle_t$ for p = " + str(p))
plt.loglog(domain, Z)

log_t = np.log(domain)
log_Z = np.log(Z)
slope_Z, init_Z = np.polyfit(log_t, log_Z, 1)
plt.title(r"$\frac{\nu}{\tau} =$ " + str(slope_Z))

plt.savefig("Z_" + str(p) + ".png")
plt.close()


# Plotting <x> logarithmicly
plt.xlabel("time")
plt.ylabel(r"$\langle x\rangle_t$ for p = " + str(p))
plt.loglog(domain, x)

log_t = np.log(domain)
log_x = np.log(x)
slope_x, init_Z = np.polyfit(log_t, log_x, 1)
plt.title(r"$\frac{\nu - \beta}{\tau} =$ " + str(slope_x))

plt.savefig("x_log_" + str(p) + ".png")
