import numpy as np, matplotlib.pyplot as plt

# Gaussian distributions
def gaussian(x, mean, variance):
    return (1 / (np.sqrt(2 * np.pi * variance))) * np.exp(-((x - mean)**2) / (2 * variance))

# Moving function
def move(x, p):
    r = np.random.rand()
    if (r < p):
        x = np.append(x, x[-1] + 1)
    else:
        x = np.append(x, x[-1] - 1)
    return x

# Generate a sample
def sample(p, T):
    x = np.zeros(1, dtype = int)
    for i in range(T):
        x = move(x, p)
    return x

# Generate a dataset
def dataset(T, N_s, p = 0.6):
    x = np.array([sample(p, T)])
    for i in range(N_s - 1):
        x = np.concatenate((x, [sample(p, T)]))
        plt.close()
    return x

# Obtain mean and variance from a dataset
def plot_data(sample, p = 0.6):
    m, v = np.mean(sample.T, axis = 1), np.var(sample.T, axis = 1)
    s = np.sqrt(v)

    # Plot mean
    domain = np.linspace(1, m.shape[0], m.shape[0])
    plt.scatter(domain, m, label = r"$\langle x(t) \rangle$", s = 1)
    plt.plot(domain, (2 * p - 1) * domain, label = r"$\langle x(t) \rangle$ Hypothesis")
    plt.xlabel(r"$t$")
    plt.ylabel(r"$x$")

    # Plot some sample trajectories
    for i in range(3):
        plt.plot(domain, sample[i], label = r"Sample Trajectory " + str(i + 1))
    plt.legend()
    plt.savefig("figures/trajectories.png")
    plt.close()

    # Plot standard deviation
    plt.scatter(domain, s, label =  r"$\langle \Delta x^2(t) \rangle^\frac{1}{2}$", s = 1)
    plt.plot(domain, np.sqrt(4 * p * (1 - p) * domain), label = r"$\langle \Delta x^2(t) \rangle^\frac{1}{2}$ Hypothesis")
    plt.legend()
    plt.xlabel(r"$t$")
    plt.ylabel(r"$x$")
    plt.savefig("figures/deviations.png")
    plt.close()

    # Get Histogram
    for i in [10, 500, 1000]:
        h = plt.hist(sample.T[i], density = True, bins = "scott")
        plt.xlabel(r"$x($t=" + str(i) + "$)$")
        plt.ylabel("Number of Occurances, normalized")
        m, v = np.mean(h[1]), np.var(h[1])
        domain = np.linspace(h[1][0], h[1][-1])
        plt.plot(domain, gaussian(domain, m, v))
        plt.savefig("figures/hist_" + str(i) + ".png")
        plt.close()

plot_data(dataset(1000, 500))