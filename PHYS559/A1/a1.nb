(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     10675,        356]
NotebookOptionsPosition[      9086,        319]
NotebookOutlinePosition[      9486,        335]
CellTagsIndexPosition[      9443,        332]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{
  RowBox[{"D", "[", 
   RowBox[{
    RowBox[{"Log", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"(", 
        RowBox[{"1", "-", "p"}], ")"}], 
       SuperscriptBox["E", 
        RowBox[{
         RowBox[{"-", "i"}], " ", "k"}]]}], "+", 
      RowBox[{"p", " ", 
       SuperscriptBox["E", 
        RowBox[{"i", " ", "k", " "}]]}]}], "]"}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"k", ",", " ", "4"}], "}"}]}], "]"}], "/.", " ", 
  RowBox[{"k", "->", "0"}]}]], "Input",
 CellChangeTimes->{{3.871214616917531*^9, 3.871214683227353*^9}, {
  3.8712147644979897`*^9, 3.871214783046526*^9}, {3.871214910175968*^9, 
  3.87121493583543*^9}, {3.871214992521627*^9, 3.871214996753867*^9}, {
  3.871215047773747*^9, 3.871215050220245*^9}, {3.8712152714628773`*^9, 
  3.871215294525655*^9}, {3.871215443060441*^9, 3.871215444439767*^9}, {
  3.871223256931799*^9, 3.871223294427456*^9}, {3.8712233367027683`*^9, 
  3.871223340910857*^9}, {3.871239625984189*^9, 3.871239630946436*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"c3bcc19f-8013-44d4-9e02-1bec0a12df95"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   SuperscriptBox["i", "4"], " ", 
   RowBox[{"(", 
    RowBox[{"1", "-", "p"}], ")"}]}], "+", 
  RowBox[{
   SuperscriptBox["i", "4"], " ", "p"}], "-", 
  RowBox[{"6", " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{"-", "i"}], " ", 
       RowBox[{"(", 
        RowBox[{"1", "-", "p"}], ")"}]}], "+", 
      RowBox[{"i", " ", "p"}]}], ")"}], "4"]}], "+", 
  RowBox[{"12", " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{"-", "i"}], " ", 
       RowBox[{"(", 
        RowBox[{"1", "-", "p"}], ")"}]}], "+", 
      RowBox[{"i", " ", "p"}]}], ")"}], "2"], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      SuperscriptBox["i", "2"], " ", 
      RowBox[{"(", 
       RowBox[{"1", "-", "p"}], ")"}]}], "+", 
     RowBox[{
      SuperscriptBox["i", "2"], " ", "p"}]}], ")"}]}], "-", 
  RowBox[{"3", " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       SuperscriptBox["i", "2"], " ", 
       RowBox[{"(", 
        RowBox[{"1", "-", "p"}], ")"}]}], "+", 
      RowBox[{
       SuperscriptBox["i", "2"], " ", "p"}]}], ")"}], "2"]}], "-", 
  RowBox[{"4", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "i"}], " ", 
      RowBox[{"(", 
       RowBox[{"1", "-", "p"}], ")"}]}], "+", 
     RowBox[{"i", " ", "p"}]}], ")"}], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"-", 
       SuperscriptBox["i", "3"]}], " ", 
      RowBox[{"(", 
       RowBox[{"1", "-", "p"}], ")"}]}], "+", 
     RowBox[{
      SuperscriptBox["i", "3"], " ", "p"}]}], ")"}]}]}]], "Output",
 CellChangeTimes->{3.871239633489012*^9},
 CellLabel->"Out[1]=",ExpressionUUID->"bbc909b2-1bda-43a8-95fd-8e37719fe87b"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Simplify", "[", 
  RowBox[{
   RowBox[{
    SuperscriptBox["i", "4"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", "p"}], ")"}]}], "+", 
   RowBox[{
    SuperscriptBox["i", "4"], " ", "p"}], "-", 
   RowBox[{"6", " ", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"-", "i"}], " ", 
        RowBox[{"(", 
         RowBox[{"1", "-", "p"}], ")"}]}], "+", 
       RowBox[{"i", " ", "p"}]}], ")"}], "4"]}], "+", 
   RowBox[{"12", " ", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"-", "i"}], " ", 
        RowBox[{"(", 
         RowBox[{"1", "-", "p"}], ")"}]}], "+", 
       RowBox[{"i", " ", "p"}]}], ")"}], "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       SuperscriptBox["i", "2"], " ", 
       RowBox[{"(", 
        RowBox[{"1", "-", "p"}], ")"}]}], "+", 
      RowBox[{
       SuperscriptBox["i", "2"], " ", "p"}]}], ")"}]}], "-", 
   RowBox[{"3", " ", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        SuperscriptBox["i", "2"], " ", 
        RowBox[{"(", 
         RowBox[{"1", "-", "p"}], ")"}]}], "+", 
       RowBox[{
        SuperscriptBox["i", "2"], " ", "p"}]}], ")"}], "2"]}], "-", 
   RowBox[{"4", " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{"-", "i"}], " ", 
       RowBox[{"(", 
        RowBox[{"1", "-", "p"}], ")"}]}], "+", 
      RowBox[{"i", " ", "p"}]}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{"-", 
        SuperscriptBox["i", "3"]}], " ", 
       RowBox[{"(", 
        RowBox[{"1", "-", "p"}], ")"}]}], "+", 
      RowBox[{
       SuperscriptBox["i", "3"], " ", "p"}]}], ")"}]}]}], "]"}]], "Input",
 NumberMarks->False,
 CellLabel->"In[2]:=",ExpressionUUID->"92f2450b-2f32-428e-b081-b308698174cd"],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "16"}], " ", 
  SuperscriptBox["i", "4"], " ", "p", " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"-", "1"}], "+", 
    RowBox[{"7", " ", "p"}], "-", 
    RowBox[{"12", " ", 
     SuperscriptBox["p", "2"]}], "+", 
    RowBox[{"6", " ", 
     SuperscriptBox["p", "3"]}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.871239636945353*^9},
 CellLabel->"Out[2]=",ExpressionUUID->"cdf5016e-3107-4965-b638-6a0e56ef72ad"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "16"}], " ", 
  SuperscriptBox["i", "4"], " ", "p", " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"-", "1"}], "+", 
    RowBox[{"7", " ", "p"}], "-", 
    RowBox[{"12", " ", 
     SuperscriptBox["p", "2"]}], "+", 
    RowBox[{"6", " ", 
     SuperscriptBox["p", "3"]}]}], ")"}]}]], "Input",
 CellLabel->"In[3]:=",ExpressionUUID->"4cf5375f-3ed6-4140-a48c-cdd7a1116269"],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "16"}], " ", 
  SuperscriptBox["i", "4"], " ", "p", " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"-", "1"}], "+", 
    RowBox[{"7", " ", "p"}], "-", 
    RowBox[{"12", " ", 
     SuperscriptBox["p", "2"]}], "+", 
    RowBox[{"6", " ", 
     SuperscriptBox["p", "3"]}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.871240453286454*^9},
 CellLabel->"Out[3]=",ExpressionUUID->"6c0ade25-8b4e-4679-a0c6-64229b63127e"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"FullSimplify", "[", 
  RowBox[{
   RowBox[{"-", "16"}], " ", 
   SuperscriptBox["i", "4"], " ", "p", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "+", 
     RowBox[{"7", " ", "p"}], "-", 
     RowBox[{"12", " ", 
      SuperscriptBox["p", "2"]}], "+", 
     RowBox[{"6", " ", 
      SuperscriptBox["p", "3"]}]}], ")"}]}], "]"}]], "Input",
 NumberMarks->False,
 CellLabel->"In[4]:=",ExpressionUUID->"f32448f8-ca81-4e58-8c38-c2f90e07c78e"],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "16"}], " ", 
  SuperscriptBox["i", "4"], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"-", "1"}], "+", "p"}], ")"}], " ", "p", " ", 
  RowBox[{"(", 
   RowBox[{"1", "+", 
    RowBox[{"6", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", "p"}], ")"}], " ", "p"}]}], ")"}]}]], "Output",\

 CellChangeTimes->{3.87124045512258*^9},
 CellLabel->"Out[4]=",ExpressionUUID->"d7f4bd2d-497a-4d13-9437-15cfc9129b69"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "1"}], "+", 
  RowBox[{"7", " ", "p"}], "-", 
  RowBox[{"12", " ", 
   SuperscriptBox["p", "2"]}], "+", 
  RowBox[{"6", " ", 
   SuperscriptBox["p", "3"]}]}]], "Input",
 CellLabel->"In[5]:=",ExpressionUUID->"0cf6e54e-b7df-474f-b678-6e0dbf631203"],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "1"}], "+", 
  RowBox[{"7", " ", "p"}], "-", 
  RowBox[{"12", " ", 
   SuperscriptBox["p", "2"]}], "+", 
  RowBox[{"6", " ", 
   SuperscriptBox["p", "3"]}]}]], "Output",
 CellChangeTimes->{3.8712404896185503`*^9},
 CellLabel->"Out[5]=",ExpressionUUID->"670d8615-ae68-4bb1-b5c0-6ec0acb6a81a"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"FullSimplify", "[", 
  RowBox[{
   RowBox[{"-", "1"}], "+", 
   RowBox[{"7", " ", "p"}], "-", 
   RowBox[{"12", " ", 
    SuperscriptBox["p", "2"]}], "+", 
   RowBox[{"6", " ", 
    SuperscriptBox["p", "3"]}]}], "]"}]], "Input",
 NumberMarks->False,
 CellLabel->"In[6]:=",ExpressionUUID->"8f232ef6-94d9-4151-9880-582dc3bf4421"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{
    RowBox[{"-", "1"}], "+", "p"}], ")"}], " ", 
  RowBox[{"(", 
   RowBox[{"1", "+", 
    RowBox[{"6", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", "p"}], ")"}], " ", "p"}]}], ")"}]}]], "Output",\

 CellChangeTimes->{3.871240491346171*^9},
 CellLabel->"Out[6]=",ExpressionUUID->"e19f4ce3-330a-4ddf-8cdc-4c5f6ef5a634"]
}, Open  ]]
},
WindowSize->{2044.5, 1339.5},
WindowMargins->{{3.75, Automatic}, {Automatic, 3.75}},
FrontEndVersion->"13.1 for Linux x86 (64-bit) (June 16, 2022)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"b6f40626-19d5-481e-af5d-0dc0b93b4e0b"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 1083, 25, 37, "Input",ExpressionUUID->"c3bcc19f-8013-44d4-9e02-1bec0a12df95"],
Cell[1666, 49, 1758, 62, 37, "Output",ExpressionUUID->"bbc909b2-1bda-43a8-95fd-8e37719fe87b"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3461, 116, 1830, 63, 33, "Input",ExpressionUUID->"92f2450b-2f32-428e-b081-b308698174cd"],
Cell[5294, 181, 450, 13, 37, "Output",ExpressionUUID->"cdf5016e-3107-4965-b638-6a0e56ef72ad"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5781, 199, 407, 12, 33, "Input",ExpressionUUID->"4cf5375f-3ed6-4140-a48c-cdd7a1116269"],
Cell[6191, 213, 450, 13, 37, "Output",ExpressionUUID->"6c0ade25-8b4e-4679-a0c6-64229b63127e"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6678, 231, 477, 14, 33, "Input",ExpressionUUID->"f32448f8-ca81-4e58-8c38-c2f90e07c78e"],
Cell[7158, 247, 469, 15, 37, "Output",ExpressionUUID->"d7f4bd2d-497a-4d13-9437-15cfc9129b69"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7664, 267, 285, 8, 32, "Input",ExpressionUUID->"0cf6e54e-b7df-474f-b678-6e0dbf631203"],
Cell[7952, 277, 330, 9, 36, "Output",ExpressionUUID->"670d8615-ae68-4bb1-b5c0-6ec0acb6a81a"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8319, 291, 351, 10, 33, "Input",ExpressionUUID->"8f232ef6-94d9-4151-9880-582dc3bf4421"],
Cell[8673, 303, 397, 13, 60, "Output",ExpressionUUID->"e19f4ce3-330a-4ddf-8cdc-4c5f6ef5a634"]
}, Open  ]]
}
]
*)

