(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      6034,        181]
NotebookOptionsPosition[      5210,        158]
NotebookOutlinePosition[      5640,        175]
CellTagsIndexPosition[      5597,        172]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"DSolve", "[", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      FractionBox[
       RowBox[{"-", "1"}], 
       RowBox[{"2", "m"}]], " ", 
      RowBox[{
       RowBox[{"y", "''"}], "[", "x", "]"}]}], "+", 
     RowBox[{"a", " ", "x", " ", 
      RowBox[{"y", "[", "x", "]"}]}]}], "\[Equal]", 
    RowBox[{"E", " ", 
     RowBox[{"y", "[", "x", "]"}]}]}], ",", 
   RowBox[{"y", "[", "x", "]"}], ",", "x"}], "]"}]], "Input",
 CellChangeTimes->{{3.87242735140556*^9, 3.872427511024993*^9}, {
  3.8725155453479443`*^9, 3.87251558944781*^9}, {3.8726137076272383`*^9, 
  3.872613737255033*^9}, {3.872613858586461*^9, 3.872613865083914*^9}, {
  3.872614012547882*^9, 3.872614021561509*^9}, {3.872615017469513*^9, 
  3.872615038180439*^9}},
 CellLabel->"In[16]:=",ExpressionUUID->"96026cf9-0c0d-473a-8dde-736084fb816b"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"y", "[", "x", "]"}], "\[Rule]", 
    RowBox[{
     RowBox[{
      RowBox[{"AiryAi", "[", 
       FractionBox[
        RowBox[{
         RowBox[{
          RowBox[{"-", "2"}], " ", "\[ExponentialE]", " ", "m"}], "+", 
         RowBox[{"2", " ", "a", " ", "m", " ", "x"}]}], 
        RowBox[{
         SuperscriptBox["2", 
          RowBox[{"2", "/", "3"}]], " ", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"a", " ", "m"}], ")"}], 
          RowBox[{"2", "/", "3"}]]}]], "]"}], " ", 
      TemplateBox[{"1"},
       "C"]}], "+", 
     RowBox[{
      RowBox[{"AiryBi", "[", 
       FractionBox[
        RowBox[{
         RowBox[{
          RowBox[{"-", "2"}], " ", "\[ExponentialE]", " ", "m"}], "+", 
         RowBox[{"2", " ", "a", " ", "m", " ", "x"}]}], 
        RowBox[{
         SuperscriptBox["2", 
          RowBox[{"2", "/", "3"}]], " ", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"a", " ", "m"}], ")"}], 
          RowBox[{"2", "/", "3"}]]}]], "]"}], " ", 
      TemplateBox[{"2"},
       "C"]}]}]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{{3.87251557366407*^9, 3.8725155958620453`*^9}, {
   3.872613714223682*^9, 3.8726137399880123`*^9}, 3.872613868487115*^9, 
   3.872614026654832*^9, {3.872615021950471*^9, 3.8726150409102993`*^9}},
 CellLabel->"Out[16]=",ExpressionUUID->"28713805-a8d3-45eb-8786-b4e28ed25c71"]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.872515539781032*^9, 
  3.872515539783464*^9}},ExpressionUUID->"b29572e0-6c7a-435c-b8b8-\
daa73ed338cb"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Series", "[", 
  RowBox[{
   RowBox[{
    SqrtBox[
     RowBox[{
      SuperscriptBox["R", "2"], "+", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"z", "-", "a"}], ")"}], "2"]}]], "-", 
    SqrtBox[
     RowBox[{
      SuperscriptBox["R", "2"], "+", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"z", "+", "a"}], ")"}], "2"]}]]}], ",", " ", 
   RowBox[{"{", 
    RowBox[{"z", ",", " ", "0", ",", " ", "3"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.872611351762843*^9, 3.87261141445786*^9}, {
  3.872611927179655*^9, 3.872611927417677*^9}, {3.872612608346875*^9, 
  3.87261265023232*^9}, {3.87261270765513*^9, 3.8726127117538843`*^9}, {
  3.8726128697094383`*^9, 3.872612874756022*^9}, {3.8726129749684772`*^9, 
  3.8726129887668953`*^9}, {3.8726146253250933`*^9, 3.872614635423691*^9}, {
  3.872614672448586*^9, 3.8726146768872433`*^9}},
 CellLabel->"In[14]:=",ExpressionUUID->"9f66c091-b1d8-4957-9e44-4f3db6caa9d6"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
   RowBox[{"-", 
    FractionBox[
     RowBox[{"2", " ", "a", " ", "z"}], 
     SqrtBox[
      RowBox[{
       SuperscriptBox["a", "2"], "+", 
       SuperscriptBox["R", "2"]}]]]}], "+", 
   FractionBox[
    RowBox[{"a", " ", 
     SuperscriptBox["R", "2"], " ", 
     SuperscriptBox["z", "3"]}], 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox["a", "2"], "+", 
       SuperscriptBox["R", "2"]}], ")"}], 
     RowBox[{"5", "/", "2"}]]], "+", 
   InterpretationBox[
    SuperscriptBox[
     RowBox[{"O", "[", "z", "]"}], "4"],
    SeriesData[$CellContext`z, 0, {}, 1, 4, 1],
    Editable->False]}],
  SeriesData[$CellContext`z, 
   0, {(-2) $CellContext`a ($CellContext`a^2 + $CellContext`R^2)^
     Rational[-1, 2], 
    0, $CellContext`a $CellContext`R^2 ($CellContext`a^2 + $CellContext`R^2)^
     Rational[-5, 2]}, 1, 4, 1],
  Editable->False]], "Output",
 CellChangeTimes->{3.872611417240814*^9, 3.872611929089094*^9, 
  3.8726129895808687`*^9, 3.872614638091848*^9, 3.8726146777071257`*^9},
 CellLabel->"Out[14]=",ExpressionUUID->"bbe3a741-dbda-4364-9d68-fabb00acb559"]
}, Open  ]]
},
WindowSize->{2044.5, 1339.5},
WindowMargins->{{3.75, Automatic}, {Automatic, 3.75}},
Magnification:>1.2 Inherited,
FrontEndVersion->"13.1 for Linux x86 (64-bit) (June 16, 2022)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"bad3a366-22f5-427d-8857-9cc1af9d9c3b"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 843, 21, 60, "Input",ExpressionUUID->"96026cf9-0c0d-473a-8dde-736084fb816b"],
Cell[1426, 45, 1448, 41, 117, "Output",ExpressionUUID->"28713805-a8d3-45eb-8786-b4e28ed25c71"]
}, Open  ]],
Cell[2889, 89, 152, 3, 35, "Input",ExpressionUUID->"b29572e0-6c7a-435c-b8b8-daa73ed338cb"],
Cell[CellGroupData[{
Cell[3066, 96, 977, 24, 44, "Input",ExpressionUUID->"9f66c091-b1d8-4957-9e44-4f3db6caa9d6"],
Cell[4046, 122, 1148, 33, 68, "Output",ExpressionUUID->"bbe3a741-dbda-4364-9d68-fabb00acb559"]
}, Open  ]]
}
]
*)

