import numpy as np
o = np.zeros((4, 4), dtype=int)
matrices = np.array([o, o])

def commutator(a, b):
    return np.matmul(a, b) - np.matmul(b, a)

def find(a):
    for i in range(len(a)):
        b = matrices[i]
        if np.array_equal(a, b):
            return i, ""
        elif np.array_equal(-a, b):
            return i, "-"

for i in range(4):
    for j in range(4):
        temp = np.zeros((4, 4), dtype=int)
        n = -1
        m = 1
        if (i * j == 0):
            n = 1
        if (i < j):
            temp[i, j] = n
            temp[j, i] = m
            matrices = np.append(matrices, [temp], axis = 0)
    
matrices = matrices[2:]
elements = ["b1", "b2", "b3", "r3", "r2", "r1"]
print("The Lorentz Group Generators:\n", matrices, "\nIts Commutations: \n")

for i in range(len(elements)):
    for j in range(len(elements)):
        if (i < j):
            result = find(commutator(matrices[i], matrices[j]))
            if result != None:
                print(elements[i], elements[j], result[1] + elements[result[0]])

