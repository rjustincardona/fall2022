function x = gepp(A,b)
% gepp.m    Gauss elimination with partial pivoting
% 
% input:  A is an n x n nonsingular matrix
%         b is an n x 1 vector
% output: x is the solution of Ax=b.
%
n = length(b);

for k = 1:n-1  
    % same as in class but only the next row needs to be modified and order of values are guarenteed
    q =k+1;
    A([k,q],k:n) = A([q,k],k:n);
    b([k,q]) = b([q,k]);
    i = k+1:n;
    A(k+1,k) = A(k+1,k)/A(k,k);  
    A(k+1,i) = A(k+1,i) - A(k+1,k)*A(k,i); 
    b(k+1) = b(k+1) - A(k+1,k)*b(k);    
end

x = zeros(n,1);
for k = n:-1:1  
    x(k) = (b(k) - A(k,k+1:n)*x(k+1:n))/A(k,k);
end
