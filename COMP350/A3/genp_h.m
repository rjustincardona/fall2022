function x = genp_h(A,b)
%
% genp.m  Gaussian elimination (no pivoting)
% input:  A is an n x n nonsingular matrix
%         b is an n x 1 vector
% output: x is the solution of Ax=b.
%
n = length(b);
for k = 1:n-1
    % same as in class but only next row needs to be changed
    i = k + 1:n;  
    mult = A(k+1,k)/A(k,k);  
    A(k+1,i) = A(k+1,i) - mult*A(k,i); 
    b(k+1) = b(k+1) - mult*b(k);  
end
x = zeros(n,1);
for k = n:-1:1  
    x(k) = (b(k) - A(k,k+1:n)*x(k+1:n))/A(k,k);
end
