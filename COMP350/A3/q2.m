% Making A and b
n = 10;
A = hilb(n);
x = ones(n, 1);


for i = 1:n
    for j = 1:n
        if (i > j + 1)
            A(i, j) = 0;
        end
    end
end
b = A * x;

format longE
% PART A
xnp = genp_h(A, b)
xpp = gepp_h(A, b)

% PART B
res_n = norm(b - A * xnp) / (norm(A) * norm(xnp))
res_p = norm(b - A * xpp) / (norm(A) * norm(xpp))

err_n = norm(x - xnp) / norm(x)
err_p = norm(x - xpp) / norm(x)
err = eps * cond(A)

% PART C
A(1, 1) = 1E-10;
b = A * x;
xnp = genp_h(A, b)
xpp = gepp_h(A, b)

res_n = norm(b - A * xnp) / (norm(A) * norm(xnp))
res_p = norm(b - A * xpp) / (norm(A) * norm(xpp))

err_n = norm(x - xnp) / norm(x)
err_p = norm(x - xpp) / norm(x)
err = eps * cond(A)
eps
