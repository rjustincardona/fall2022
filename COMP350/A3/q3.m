format longE
n = 1000;
A = ones(n);

for i = 1:n
    for j = 1:n
        A(i, j) = abs(i - j);
    end
end
DET(A)

n = 1200;
A = ones(n);

for i = 1:n
    for j = 1:n
        A(i, j) = abs(i - j);
    end
end
DET(A)
