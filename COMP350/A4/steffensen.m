function r = steffensen(func, x, xtol, ftol, nmax)
    R = roots([-6, 15, 10, -30, 0, 5]);
    R = R(4);
    for n = 1:nmax
        fx = func(x);
        gx = (func(x + fx) - fx) / fx;
        d = fx / gx;
        fprintf("iter: %d, %23.15e, %23.15e\n", n, x, abs(x - R) / abs(R))
        xnew = x - d;
        if isnan(xnew) | isinf(xnew)
            r = x;
            return
        end
        x = xnew;
        if abs(fx) <= ftol | abs(d) <= xtol
            r = x;
            return
        end
    end
    r = x;
end

