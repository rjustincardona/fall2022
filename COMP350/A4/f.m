function y = f(x)
    x2 = x * x;
    x3 = x * x2;
    x4 = x * x3;
    x5 = x * x4;
    y = 5*(3*x4 - 6*x2 + 1) - 2*(3*x5 - 5*x3);
end