xtol = 1e-14;
ftol = 0;
nmax = 20;
x0 = [0, 0.1, 0.2, 0.3, 0.4, 0.5];
R = roots([-6, 15, 10, -30, 0, 5]);
R = R(4);

fprintf("Steffensen\n");
for k = 1:length(x0)
    a = steffensen(@f, x0(k), xtol, ftol, nmax);
    fprintf("iter: %d, %23.15e, %23.15e\n", k, a, abs(a - R) / abs(R))
end

fprintf("Newton\n");
for k = 1:length(x0)
    a = newton(@f, @fd, x0(k), xtol, ftol, nmax, 0);
    fprintf("iter: %d, %23.15e, %23.15e\n", k, a, abs(a - R) / abs(R))
end

fprintf("Secant\n");
for k = 2:length(x0)
    a = secant(@f, x0(k - 1), x0(k), xtol, ftol, nmax);
    fprintf("iter: %d, %23.15e, %23.15e\n", k, a, abs(a - R) / abs(R))
end
