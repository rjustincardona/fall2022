function r = secant(f, x0, x1, xtol, ftol, nmax)
    R = roots([-6, 15, 10, -30, 0, 5]);
    R = R(4);

    fx0 = f(x0);
    fx1 = f(x1);

    for n = 1:nmax
        d = ((x1 - x0) ./ (fx1 - fx0)) .* fx1;
        x0 = x1;
        fx0 = fx1;
        x1 = x1 - d;
        fx1 = f(x1);
        fprintf("iter: %d, %23.15e, %23.15e\n", n, x1, abs(x1 - R) / abs(R))
        if abs(d) <= xtol | abs(fx1) < xtol
            r = x1;
            return
        end
    end
end