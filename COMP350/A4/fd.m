function y = fd(x)
    x2 = x * x;
    x3 = x * x2;
    x4 = x * x3;
    y = 60*(x3 - x) - 30*(x4 - x2);
end