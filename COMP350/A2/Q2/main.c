#include <stdio.h>
#include <stdlib.h>
#include <math.h>


// Directly applying the given recursion for part a
float a(int n){
    if (n == 2){
        return 2.0 * sqrt(2.0);
    }
    float p = pow(2.0, n - 1.0);
    return p * sqrt(2.0 * (1.0 - sqrt(1.0 - pow(a(n - 1.0) / p, 2.0))));

}


// Using a better recursion for part b
float b(int n){
    if (n == 2){
        return 2 * sqrt(2);
    }
    return b(n - 1) * sqrt(2 / (1 + sqrt(1 - pow(b(n - 1) / pow(2, n - 1), 2))));

}


int main(){
    // Printing result from part a
    printf("PART A:\n");
    for (int i = 3; i <= 20; i++){
        printf("%d: %e\n", i, a(i) - M_PI);
    }


    // Printing result from part b
    printf("\nPART B:\n");
    for (int i = 3; i <= 20; i++){
        printf("%d: %e\n", i, b(i) - M_PI);
    }

    return 0;
}
