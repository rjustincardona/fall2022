#include <stdio.h>
#include <math.h>
#include <float.h>


// Representation of a number in the mantissa (m) and exponent (e) format.
struct rep{
    float m;
    float e;
};


// Gets the representation of a float
struct rep get_rep(float a){
    struct rep b;
    int temp_e;
    b.m = frexp(a, &temp_e);
    b.e = (float) temp_e;
    return b;
}


// Multiplies two representations together and returns the representation of the product
struct rep mult(struct rep a, struct rep b){

    // Initialzing rep to be returned
    struct rep c;

    // Calculating the product
    float e = a.e + b.e;
    struct rep m = get_rep(a.m * b.m);

    // Adding the two exponents and assigning the new mantissa/exponent
    c.m = m.m;
    c.e = e + m.e;
    return c;
}


int main (){
    
    // The floats to be multiplied
    float y[] = {4, 3E-30, 2E-30};
    int y_size = (int) (sizeof(y) / sizeof(y[0]));

    // Iterate through the list, multiplying each one
    struct rep product = mult(get_rep(1), get_rep(1));
    float prod = 1;
    int exp = 0;
    for (int i = 0; i < y_size; i++){
        if ((y[i] * prod == INFINITY) || ((y[i] != 0.0) && (prod != 0.0) && (y[i] * prod == 0.0))){
            int e = floor(log10(y[i]));
            float m = y[i] / pow(10, e);
            exp += e;
            prod *= m;

            printf("bad: %e, %e\n", prod, e);
        }
        else{
            prod *= y[i];
        }
        printf("%e\n", prod);
        //printf("%f, %d\n", prod, exp);
        //product = mult(product, get_rep(y[i]));
    }

    // Convert from powers of 2 to powers of 10
    //float e_full = product.e * log10(2);
    //int e = (int) e_full;
    //float fac = pow(10, e_full - (float) e);

    // Print the result
    //printf("%e times ten to the power %d", fac * product.m, e);
    return(0);
}
