#include <stdio.h>
#include <math.h>
#include <float.h>
#include <stdbool.h>


// Representation of a number in the mantissa (m) and exponent (e) format.
struct rep{
    float m;
    float e;
};


// Gets the representation of a float
struct rep get_rep(float a){
    struct rep b;
    int temp_e;
    b.m = frexp(a, &temp_e);
    b.e = (float) temp_e;
    return b;
}


// Multiplies two representations together and returns the representation of the product
struct rep mult(struct rep a, struct rep b){

    // Initialzing rep to be returned
    struct rep c;

    // Calculating the product
    float e = a.e + b.e;
    struct rep m = get_rep(a.m * b.m);

    // Adding the two exponents and assigning the new mantissa/exponent
    c.m = m.m;
    c.e = e + m.e;
    return c;
}


int main (){
    
    // The floats to be multiplied
    float y[] = {4.2, 1E+30, 2, 1, 5};
    int y_size = (int) (sizeof(y) / sizeof(y[0]));

    // Iterate through the list, multiplying each one
    struct rep product = mult(get_rep(1), get_rep(1));
    float prod = 1;
    bool is_zero = false;
    bool is_normal = true;
    for (int i = 0; i < y_size; i++){
        if (y[i] == 0){
            printf("0 times ten to the power 0");
            return 0;
        }
        prod *= y[i];
        if (isinf(prod)){
            is_normal = false;
            break;
        }
        if (prod == 0){
            is_normal = false;
            break;
        }
    }
    // If there was no under/overflow print the usual result, if not using the new method
    if (is_normal){
        printf("%e times ten to the power 0", prod);
        return 0;
    }
    for (int i = 0; i < y_size; i++){
        product = mult(product, get_rep(y[i]));
    }

    // Convert from powers of 2 to powers of 10
    float e_full = product.e * log10(2);
    int e = (int) e_full;
    float fac = pow(10, e_full - (float) e);

    // Print the result
    printf("%f times ten to the power %d", fac * product.m, e);
    return(0);
}
