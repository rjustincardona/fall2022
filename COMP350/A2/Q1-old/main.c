#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Multiplies two numbers together in the string format
char* multiply(char* a, char* b){
    // Creating the output array with the maximum number of digits needed
    int al = strlen(a);
    int bl = strlen(b);
    int cl = al + bl;
    int* c[cl];
    for (int i = 0; i < cl; i++){
        c[i] = 0;
    }

    // Indices for the place values of the digits being multiplying
    int i1 = 0;
    int i2 = 0;

    // Multiplying
    for (int i = al - 1; i >= 0; i--){
        int carry = 0;
        int n1 = a[i] - '0';
        i2 = 0;

        for (int j = bl - 1; j >=0; j--){
            int n2 = b[j] - '0';
            int sum = (n1 * n2) + ((int)c[i1 + i2]) + carry;
            carry = sum / 10;
            c[i1 + i2] = sum % 10;
            i2++;
        }
        // Carrying if needed
        if (carry > 0){
            c[i1 + i2] = (int)c[i1 + i2] + carry;
        }
        i1++;
    }

    // Finding the number of leading zeroes
    int rl = cl;
    for (int i = cl - 1; i >=0; i--){
        if ((int)c[i] == 0){
            rl--;
        }
        else {
            break;
        }
    }

    // Removing the leading zeroes
    char* result = (char*) malloc(rl * sizeof(char));
    for (int i = 0; i < rl; i++){
        result[i] = '0';
    }
    for (int i = rl - 1; i >= 0; i--){
        result[rl - i - 1] = (char)c[i] + '0';
    }

    return result;
}

// Struct in order to represent numbers as a natural number times a power of 10
struct num{
    int e;
    char* m;
};

// Removes decimal from number and returns the power of ten
struct num dec(char* a){

    // Determining length of new string and identifying the pwoer of 10
    int al = strlen(a);
    int exp = 0;
    for (int i = al - 1; i >= 0; i--){
        int a_temp = a[i];
        if (a_temp == '.'){
            exp = al - i - 1;
            break;
        }
    }

    // Making the new mantissa without the decimal
    char* result = (char*) malloc(al * sizeof(char));
    int r_i = 0;
    for (int i = 0; i < al; i++){
        int a_temp = a[i];
        if (a_temp != '.'){
            result[r_i] = a_temp;
            r_i++;
        }
    }
    struct num r;
    r.e = exp;
    r.m = result;
    return r;

}


// The program assumes that the inputs are provided as command line argumnts in the decimal format
int main(int argc, char *argv[]){

    // Multiplying each of the supplied numbers in series
    char* r = "1";
    int exponent = 0;
    for (int i = 1; i < argc; i++){
        // Removing the decimal in order to multiply
        struct num n = dec(argv[i]);
        exponent -= n.e;
        r = multiply(r, n.m);
    }

    // Adding back the exponent to represent in the desired format
    exponent += strlen(r) - 1;
    printf("%c.", r[0]);
    for (int i = 1; i < strlen(r); i++){
        printf("%c", r[i]);
    }
    printf(" times 10 to the power %d", exponent);
    return 0;
}
