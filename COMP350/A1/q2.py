x = 0;
for k in range(16):
    if (k == 1 or k == 3 or k == 4):
        continue
    elif (k == 15):
        x -= 2**(k)
    else:
        x += 2**k

print(x)
