import numpy as np
import matplotlib.pyplot as plt

def p1(x):
    return 1 / (1 + 0.003 * ((1 + x) / (1 - x)**2))


def p2(x):
    return 1 / (1 + 0.003 * (1 + x) * (((1 - x)**2 - 0.01 * x) / ((1 - x)**2 + 0.01)**2))

dom = np.linspace(0, 2, 100)

plt.plot(dom, p1(dom), label = r"$\gamma = 0$")
plt.plot(dom, p2(dom), label = r"$\gamma = 0.1\omega_0$")
plt.legend()
plt.title("Graphs for question 9.26")
plt.savefig("9_26_graphs.png")
