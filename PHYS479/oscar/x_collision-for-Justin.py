#!/usr/bin/env python
# coding: utf-8
"""
Created on Thu Sep 23 13:13:02 2021

@author: oscar hernandez
"""
# ### 2021 Python version of 2013 Mathematica notebook used to calculate $x_c$ and $x_\alpha$ for my PRD 2014 paper, ``Wouthuysen-Field absorption trough in cosmic string wakes''
# x_coll coefficients only, all x_alpha stuff removed

# In[1]:


import numpy as np
import matplotlib.pyplot as plt #equivalent to: from matplotlib import pyplot as plt
pi=np.pi;


# ### Physical Constants Imported

# In[2]:


# https://codata.org/initiatives/data-science-and-stewardship/fundamental-physical-constants/
# https://physics.nist.gov/cuu/Constants/index.html


# In[3]:


#I have here two separate files for names and values, respectively
#but I decided to go with only one file which contains that info.
#
#values=np.loadtxt('data_from_mathematica/PhysicalConstants/constants.dat')
#names=np.loadtxt('data_from_mathematica/PhysicalConstants/constantsNames.dat',dtype=str)
#for n in range(len(names)):
#    locals()[names[n]] = values[n]
#
#pwd='/Users/oscar/OneDrive - McGill University/2021_09_01-WFabsorption/'

names_values=np.loadtxt('data_from_mathematica/PhysicalConstants/constantsNamesAndValues.dat',dtype=str)
for n in range(np.shape(names_values)[0]):
    locals()[names_values[n,0]]=names_values[n,1].astype(float)
    
names_values


# In[4]:


#Hubble in units of 1/seconds for h=0.7 . We can change h here, if we wish
Ho *= 0.7/0.7


# ### Hubble, Hydrogen density, Temperatures, Optical Depth

# In[5]:


OmegaL = (1-Omegam-OmegaR)

def HubbleFull(z):
    return Ho*np.sqrt(Omegam*(1+z)**3 + OmegaR*(1+z)**4 + OmegaL)

def HmatterLambda(z):
    return Ho*np.sqrt(Omegam*(1+z)**3 + (1-Omegam))

def Hmatter(z):      #this is what I used in Mathematica calculation
    return Ho*np.sqrt(Omegam*(1+z)**3)


# In[7]:


zz=np.linspace(5,30,50)
plt.plot(zz,1-Hmatter(zz)/HubbleFull(zz),'xg',label='Hmatter error')
plt.plot(zz,1-HmatterLambda(zz)/HubbleFull(zz),'.b',label='HmatterLambda error')
plt.plot(zz,1-Hmatter(zz)/HmatterLambda(zz),'vr',label='1-Hmatter/HmatterLambda')
#plt.yscale('log')
plt.legend()
plt.title('diff Hubble vs z')
plt.xlabel('redshift z')
plt.ylabel('diff Hubble')
plt.show()


# In[9]:


#Decide here which Hubble to use
def Hubble(z):
    return Hmatter(z) #this is what 2013 Mathematica calculation used
    #return HmatterLambda
    #return HubbleFull(z)

deltab=0 
nbaryon=(1+deltab)
def nH(z):
    return (1-Y)*Omegab*(1+z)**3 * nbaryon*energycritical/(mH*c**2)

def Tgamma(z):
    return Tcmb*(1+z)

#def TK(z):
#    if z < 135.25:
#        TK=0.02*(1+z)**2
#    else:
#        TK=Tgamma(z)
#    return TK

def TK(z):
    return 0.02*(1+z)**2

def TC(z):   #this is the approximation I made in my 2013 work
    return TK(z) 

#The optical depth for Lyman alpha photons is given by 
#the Gunn-Peterson optical depth τGP ≈ 2e4 xHI (z+1)^(3/2) 
#Before reionization is significant (xHI not being small), 
#the large τ value means that TC is driven to TK of the IGM. 
#We work with xHI close to 1 and we take TC ≈ TK.


#x_coll and x_alpha both depend on redshift z and I could have defined TS(z) only
#with the xc(z) and xalpha(z) functions, but then I would have had to wait until after 
#they are defined to define TS. 
def TS(z,x_coll,x_alpha):
    return (1+x_coll+x_alpha)/(1/Tgamma(z)+x_coll/TK(z)+x_alpha/TC(z))

def tau21(z, x_coll, x_alpha, xHI=1, dvpecdr=0):
    optical_depth = (3*hbar * c**3 * A10)/(16*k * nu21**2)
    optical_depth *= xHI * nH(z) / Hubble(z)
    optical_depth *= (1/TS(z,x_coll, x_alpha ))*(1/(1+(1+z)*dvpecdr*Hubble(z)))
    return optical_depth

'''
def dTb21(z, x_coll, x_alpha, xHI=1, dvpecdr=0):
    tau=tau21(z, x_coll, x_alpha, xHI, dvpecdr)
    dTb21=[]
    for t in tau:
        if t < 0.15:
            dTb21.append(t)
        else:
            dTb21.append(1-np.exp(-t))
            #print('optically thick',t,'>0.15')
    dTb21 *= (TS(z,x_coll,x_alpha) - Tgamma(z))/(z+1)
    return np.array(dTb21)
'''

def dTb21(z, x_coll, x_alpha, xHI=1, dvpecdr=0):
    tau=tau21(z, x_coll, x_alpha, xHI, dvpecdr)
    dTb21 = 1-np.exp(-tau)
    dTb21 *= (TS(z,x_coll,x_alpha) - Tgamma(z))/(z+1)
    return dTb21


# In[10]:


#compare with analytic result eq 9 in my 2014 WF paper. Everything is OK
print('should be 9.07535e-3  ', tau21(0, 1,0)*TS(0,1,0))

def dTb21analytic(z, x_coll, x_alpha):
    shouldbe136=Tcmb/TK(0)
    return 9.075e-3 * np.sqrt(1+z)*(x_coll+x_alpha)/(1+x_coll+x_alpha)*(1-shouldbe136/(1+z))

#compare z dependence
zzz=np.linspace(5,100,100)
dTbdiff=1-dTb21(zzz,1,0)/dTb21analytic(zzz,1,0)
plt.plot(zzz,dTbdiff,'.')
plt.title('dTbdiff vs z with x_coll=1, x_alpha=0')
plt.show()

#compare coefficient dependence
xxx=np.linspace(0,1,50)
dTbdiff2=1-dTb21(0,xxx,0)/dTb21analytic(0,xxx,0)
plt.plot(xxx,dTbdiff2,'.')
plt.title('dTbdiff vs x_coll with z=0, x_alpha=0')
plt.show()


# ### xc collision coefficient
# My Mathematica work is in 'coefficients/CollisionCoeffAnalyticApprox.nb'
# #### xe_a.dat  ionization coefficient table 
# #### kappa-HH.cvs collision coefficients table, in cm$^3$/s (need to change to $10^{-6}$ m$^3$/s here and below)
# values from Table 3 in Furlanetto, Oh, Brigg Phys.Rep 2006 from
# B. Zygelman, Astrophys. J. 622 (2005) 1356–1362.
# #### kappa-eH.txt collision coefficients table, in cm$^3$/s 
# values from Table 4 in Furlanetto, Oh, Brigg Phys.Rep 2006 from
# S.R. Furlanetto, M.R. Furlanetto, Mon. Not. R. Astron. Soc. 374 2007 (astro-ph/0608067).
# My kappa-eH.txt table goes up to 20,000K (as does the tables of my references) and agrees exactly with 21cmFAST for those values but the 21cmFAST table kappa_eH_table.dat goes up to 100,000K
# #### Equations
# $
# n_e=x_e n_H
# ~~~,~~~
# n_{\text{HI}}=\left(1-x_e\right) n_H
# $
# 
# $
# x_{\text{coll}}=x_{\text{eH}}+x_{\text{HH}}
# ~~~,~~~
# x_{\text{eH}}=\frac{T_{\text{star}} \left(n_e \kappa _{\text{eH}}\right)}{A_{10} T_{\gamma }}
# ~,~
# x_{\text{HH}}=\frac{T_{\text{star}} \left(\kappa _{\text{HH}} n_{\text{HI}}\right)}{A_{10} T_{\gamma }}
# $

# In[9]:


#input table of free electrons as a function of 'a'
xe_a=np.loadtxt('data_input/xe_a.dat')

def ixe(z):
    a=1/(z+1)
    return np.interp(a,xe_a[:,0],xe_a[:,1]) #interpolation needs x argument to be increasing


# In[10]:


plt.loglog(-1+1/xe_a[:,0],xe_a[:,1],'.',label='xe vs z')
#input table of free electrons as a function of 'a'
zz=np.logspace(-4,4,100)
plt.loglog(zz,ixe(zz),label='interpolated xe vs z')
plt.xlabel('z')
plt.ylabel('xe')
plt.legend()
plt.show()


# In[11]:


kappa_eH=np.loadtxt('data_input/kappa-eH.txt')
kappa_HH=np.loadtxt('data_input/kappa-HH.csv',delimiter=',')


# In[12]:


plt.loglog(kappa_eH[:,0],kappa_eH[:,1],'o',label='kappa_eH')
plt.loglog(kappa_HH[:,0],kappa_HH[:,1],'x',label='kappa_HH')
plt.xlabel('TK')
plt.ylabel('kappa')
plt.legend()
plt.show()


# In[13]:


#use log(TK) for interpolation as recommended by Furlanetto and Furlanetto
def ikappa_eH(TK):
    logKappa=np.log(kappa_eH)
    kappa  = np.exp(np.interp(np.log(TK),logKappa[:,0],logKappa[:,1]))
    kappa *= 10**(-6)  # change from cm**3 to m**3
    return kappa

def ikappa_HH(TK):
    logKappa=np.log(kappa_HH)
    kappa  = np.exp(np.interp(np.log(TK),logKappa[:,0],logKappa[:,1]))
    kappa *= 10**(-6)  # change from cm**3 to m**3
    return kappa

# xc Collision coefficient
#
def xc(z):
    xc  = ixe(z)*ikappa_eH(TK(z)) + (1-ixe(z))*ikappa_HH(TK(z))
    xc *= Tstar*nH(z)/(A10*Tgamma(z))
    return xc 


# In[14]:


#Compare to Mathematica notebook results from z=7 to z=30
xc_mathematica=np.loadtxt('data_from_mathematica/xc_z7-z30.dat')
zzz=np.linspace(7,30,24)
plt.plot(zzz,xc_mathematica,'o',label='xc_mathematica(z)')
plt.plot(zzz,xc(zzz),'v',label='xc(z)')
plt.xlabel('z')
plt.ylabel('xc')
plt.legend()
plt.show()

plt.plot(zzz,abs(xc_mathematica/xc(zzz)-1),'o',label='xc % diff')
#plt.yscale('log')
plt.legend()
plt.title('comparison of python and mathematica calculation')
plt.show()
