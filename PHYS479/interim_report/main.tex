\documentclass[twocolumn,twoside]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{flafter}
\usepackage{graphicx}
\usepackage{setspace}
\usepackage{mathtools}
\usepackage{listings}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage[margin=0.5in]{geometry}
\usepackage{titling}
\usepackage{hyperref}
\pretitle{\begin{flushleft}\LARGE}
    \posttitle{\par\end{flushleft}\vskip 0.5em}
    \preauthor{\begin{flushleft}\large \lineskip 0.5em}
    \postauthor{\par\end{flushleft}}
    \predate{\begin{flushleft}\large}
    \postdate{\par\end{flushleft}}
\title{\textbf{PHYS 479 Interim Report: On the Emulation and Inversion of The Accelerated Reionization Era Simulations}}
\author{Justin Cardona\\{\small Supervisor: Oscar Hernandez}}

\begin{document}
    \maketitle
    \section*{Abstract}
    Programs such as the Accelerated Reionization Era Simulations (ARES) are some of the most important tools cosmologists have for depicting the evolution of the early universe. Unfortunately, obtaining exact time evolution of the state of complex systems can be computationally expensive. ARES is a codebase made to create models of the 21-cm signal in a time efficient manner \cite{ARES}. Given some cosmological parameters that characterize the early universe, it can output the corresponding global 21-cm signal. The goal of this project is to investigate alternative techniques that are more time efficient while remaining accurate. Here, various numerical methods are compared as emulators of ARES, going from parameters to the 21-cm signal; and inverting it, going from the 21-cm signal to parameters. The methods are Radial Basis Function Interpolation (RBFI), Multi-Layer Perceptron Neural Networks (MLP), and the Neuro-Evolution of Augmenting Topologies (NEAT). It is found that RBFI and MLPs are great for emulation and inversion while NEAT is not.

    \section{Introduction}

    First, a brief summary of the early universe will be provided. This is to motivate the importance of understanding the behaviour of $\delta T_b$. Following the big bang, the universe is thought to be at very large temperature and therefore was dominated by relativistic particles. Due to the expansion of spacetime, temperatures cooled and the energy density of radiation decreases more than that of matter due to redshift.
    
    This causes the universe to be dominated by homogeneously distributed baryonic matter at by redshift $z\approx 1100$ relative to the big bang. Expansion decreases the density and temperature of baryonic matter, allowing neutral hydrogen to form. Thus, this era is named Recombination. Due to the hyperfine splitting of hydrogen, light with a 21cm wavelength is emitted, and since Recombination it has redshifted (the CMB). Small inhomogeneities begin to form and dark baryonic matter (non-interact with the electromagnetic force) does not experience pressure so gravitational wells form. Normal baryons are attracted by these wells but are also repelled by pressure. They undergo oscillation and since the baryons move slower away from the wells, they spend less time close to them. This causes less light to be emitted from the underdense regions, and this is visible in the CMB.

    Inflation continues to exert its influence, and decreasing densities lead to a decrease in compton scattering. As a result, the kinetic temperature of the matter $T_k$ becomes decoupled from the CMB temperature $T_\gamma$. However, the Wouthuysen-Field (WF) effect now couples $T_\gamma$ to the spin temperature $T_s$. It is defined through the ratio of singlet to triplet state hydrogen: $\frac{n_1}{n_0} = 3e^{\frac{-\Delta E}{T_s}}$. Now, as stars and other astronomical object begin to form, more energetic processes occur and $Ly\alpha$ photons are produced in significant amounts. These emmisions shift the ratio of hydrogen states, and couple the spin temperature to the CMB temperature. In addition to $Ly\alpha$ photons, X-rays are also produced and this has the effect of increasing temperatures once again. In the end, as all this energetic light is emitted, more and more hydrogen is ionized (Reionization). There is therefore less neutral hydrogen so finally $T_s$ decouples entirely from $T_k$. 
    
    The differential brightness temperature is given by $\delta T_b = \frac{T_s-T_\gamma}{1+z}(1-e^{-\tau})$ where $\tau$ is the optical depth of absorption. Effectively, this measures how coupled $T_s$ is to $T_\gamma$. As seen above, how coupled these two quantities are is very important for determining the evolution of different species of matter in the early universe. It will thus be the focus in the use of the various computational tools here. In the Background section, the workings of each of the programs used will be summarized in their respective subsections. In the Method and Results section, the specifics on the implementations of these programs, the computational setup, and the results of using these methods are presented. An interpretation of these results is given in the Discussion and finally the Conclusions and further possible work are presented.

    \section{Background}
    \subsection{ARES}
    In this section, how ARES computes $\delta T_b(z)$ will be discussed. $\delta T_b$ can be approximated with the spin temperature $T_s$ as \cite{f2006}:
    \begin{align}
        \delta T_b&\approx 27(1-\bar{x}_i)\left(\frac{\Omega_{b, o}h^2}{0.023}\right)\sqrt{\frac{0.15}{\Omega_{m,o}h^2}\frac{1+z}{10}}\left(1 - \frac{T_\gamma}{T_s}\right)\\
        T_s^{-1} &= \frac{T_\gamma^{-1} + x_cT_k^{-1}x_\alpha T^{-1}_\alpha}{1+x_c+x_\alpha}
        \label{dTb}
    \end{align}
    Here, the color temperature $T_\alpha\approx T_k$, the kinetic temperature \cite{f2006}. $T_\gamma$ is the CMB temperature, $x_c$ is the collisional coupling coefficient and in the ARES code appears as a tabulated result calculated by Zygelman\cite{Zygelman}, and $x_\alpha = 1.81\times 10^{11}\frac{\hat{J}_\alpha}{1 + z}$. $T_k$ is found via the heating rate density $\epsilon_X$ \cite{m2012}:
    \begin{align}
        \frac{3}{2}&\frac{d}{dt}\left(\frac{k_bT_kn_{tot}}{\mu}\right) = \epsilon_X + \epsilon_\text{comp} - C\\
        \epsilon_X &= 4\pi\sum_jn_j\int_{\nu_\text{min}}^{\nu_\text{max}}f_\text{heat} \hat{J}_\nu \sigma_{\nu, j}(h\nu - h\nu_j)d\nu
    \end{align}
    $\epsilon_\text{comp}$ is the compton heating rate density, and $C$ is the cooling rate from all factors. This is computed in the code using results from Furlanetto \& Stoever \cite{fs2010}. In order to obtain $\hat{J}$, one must solve the cosmologival radiative transfer equation \cite{m2012}:
    \begin{multline}
        (\partial_t - \nu H(z)\partial_\nu)\hat{J_\nu}(z) + 3H(z)\hat{J_\nu}(z) = -c\alpha_\nu J_\nu(z) \\+ \frac{c}{4\pi}\epsilon_\nu(z)(1+z)^3
    \end{multline}
    $\alpha_\nu$ is the absorption coefficient and is related to optical depth by $d\tau_\nu = \alpha_\nu ds$ for a path length ds. The emissivity is modelled as follows \cite{m2012}:
    \begin{align}
        \epsilon_\nu &= \rho_n^{-o}c_if_i\frac{df_\text{coll}}{dt}I_\nu\\
        f_\text{coll} &=\rho_m^{-1}(z)\int_{m_\text{min}}^\infty mn(m)dm
    \end{align}
    Here, $\rho_b^{-o}$ is the mean baryon density today, $c_i$ is a normalization factor for baryonic collapse into energy output in some emission band $i$ (e.g. Ly$\alpha$, soft UV, X-ray), and $f_i$ characterizes its uncertainty when evolving with redshift. $f_\text{coll}$ is the fraction of matter collapsed haloes found in stars more massive than $m_\text{min}$, $n(m)$ being the probability distribution of the mass of stars. This is obtained using the \href{https://github.com/halomod/hmf}{hmf} code \cite{hmf}. $I_\nu$ gives the SED of astrophysical sources, and is a power law here \cite{m2012}.

    \subsection{RBFI}
    One might think of the more well-know polynomial interpolation to accomplish this task, but this requires that the interpolation points lie on a structured grid. Due to the fact that the parameter space being explored can span many orders of magnitude, this will not be a suitable method. In addition, in $D$ dimensions, by the  Mairhuber-Curtis theorem for $D\ge 2$ any any open set containing the interpolation points there exist $D$ points for which the interpolation matrix is singular \cite{m1956}. In order to circumvent this, a method that is that has basis function dependant of the positions of the interpolands is used (they are chosen to be positive definite to maintain the non-singularity of the matrix).

    The method is as follows. Consider $N$ interpolation points $x_k\in\mathbb{R}^D$ for $1\le k\le N$ and $N, D\in\mathbb{N}$. Let $f:\mathbb{R}^D\rightarrow \mathbb{R}$ be the function to be interpolated. The function $h$ that interpolates $f$ is given as \cite{m1956}:
    \begin{equation}
        h = \sum_{k=0}^N a_k g(\|x - x_k\|),
        \label{rbfi_def}
    \end{equation}
    where, given some positive definite $g$, the $a_k$'s are formed as below, $\chi_{ij}\equiv \|x_i-x_j\|$, :
    \begin{equation}
        \begin{pmatrix*}
            g(\chi_{00}) & g(\chi_{10}) & \dots & g(\chi_{N0})\\
            g(\chi_{01}) & g(\chi_{11}) & \dots & g(\chi_{N1})\\
            \vdots & \vdots & \ddots & \vdots\\
            g(\chi_{0N}) & g(\chi_{1N}) & \dots & g(\chi_{NN})
        \end{pmatrix*}\begin{pmatrix*}
            a_0\\ a_1\\\vdots\\a_N 
        \end{pmatrix*}= \begin{pmatrix*}
            f(x_0)\\f(x_1)\\\vdots\\f(x_N)
        \end{pmatrix*}
    \end{equation}
    The above linear system of equation is solved for the coefficient so that the form of $h$ can be found. If $f$'s output is supposed to be multi-dimensional, then this process can be done for each dimension.
    \subsection{MLP}
    Consider some map $f:\mathbb{R}^N\rightarrow\mathbb{R}^M$. A multilayer perceptron attempts to mimic this function by constructing a directed acyclic graph. This graph consists of disjoint subsets of nodes (hereafter refered to as layers) with these properties \cite{nn}:
    \begin{itemize}
        \setstretch{0.1}
        \item There are 2 special layers called the input and the output layers which respectively have $N$ and $M$ nodes. The other layers can have any number of elements and are called ``hidden''.
        \item Given that a MLP has $L$ layers, the layers can be assigned indices 1 through $L$. The input and output layers getting 1 and $L$.
        \item For any layer $i$, $L\ge i \ge 2$, a subgraph can be formed with either of layers $i-1$ or $i+1$. Each of these subgraphs is a complete bipartite one, with each partition being each layer. The edges point towards the node whose layer has a greater index.
        \item There are no other edges than those required above.
        \setstretch{1}
    \end{itemize}
    Each node is given an input value, an output value, and an activation function $a:\mathbb{R}\rightarrow\mathbb{R}$ (It is common that sigmoids are used) \cite{nn}. Each edge is given an input value, an output value, and a weight. When a value is ``passed though'' a node, the output is assigned the value of the activation function evaluated at the input. If there are any edges that point away from the node, their inputs are set to the node's output. Whenever an edge receives an input, it multiplies it by its weight and sets that as its output. This output is then added to the input of the node it points to.

    Evaluating a set of numbers with an MLP is done as follows, the inputs of all nodes are set to zero. Then the input numbers are passed through the input nodes, each node receives one number. Finally for each layer $2$ through $L$ (in increasing order), the inputs of each of the nodes are passed through. The final layer's outputs are the outputs of the MLP \cite{nn}.

    In order for this method to actually mimic another function in must train to do so. This is done by evaluating the MLP multiple times for multiple different inputs. Each time the MLP runs, the error of its output can be computed. Based on the severity of the errors, the weights of the edges are changed until the neural network (NN) has an acceptable error margin or is not expected to improve further. There are many methods commonly used for this, and will be discussed further in section 3.
    
    \subsection{NEAT}
    NEAT is a genetic algorithm for creating NNs that addresses an important flaw of MLPs \cite{NEAT}. While methods such as stochastic gradient descent (SGD) or Limited-Memory Broyden-Fletcher-Goldfarb-Shanno algorithm (LBFGS) can change the weights of the edges, there is not much else that can be accomplished. The issue here is that it is possible that the NN might be insufficiently complex for the task at hand \cite{NEAT}. Another disadvantage of the MLP method is that the time complexity for evaluating scales with the number of edges and nodes. This does not change in NEAT, but when the topology of the NN (refered to as an organism in this setting) more complex than needed, time is wasted during evaluation and training \cite{NEAT}.

    NEAT addresses these problems by making the topology variable. Initially, an organism just has input and output layers. After evaluating, it may probabilistically change in some way (mutate). Adding/removing nodes and edges (``genes'' in general), changing the weights of edges, changing activation functions of nodes, and other features that depend on the implementation are mutation examples. NEAT begins with copies of the trivially simple network descibed earlier. The organisms compete for a series of generations. Each generation, organisms are evaluated and ranked using a fitness function. The best organisms will compete in the next generation and the worse are replaced with mutations of the best organisms as well as their offspring \cite{NEAT}.

    \begin{figure}[h!]
        \includegraphics[width = \linewidth]{offspring.png}
        \caption{Diagram of offspring production from Stanley's paper \cite{NEAT}. Parents are represented with a series of edges that have been added (with its innovation number at the top of each box). They are then ``lined up'' by increasing number to create an offspring with a non-problematic topology.}
        \label{offspring}
    \end{figure}
    
    NEAT combines organisms into offspring by keeping track of a \textit{global innovation number}, which is incremented and assigned to a gene when it is created. (See Figure \ref{offspring}). This offers a significant advatage in time complexity when compared to doing graph-theoretic analyses of the organisms \cite{NEAT}. After many generations, the best organism is selected. It is in this way that the NEAT algorithm both steers the organisms towards fitness while allowing for more diversity than MLPs. Note that since the networks start in a minimal configuration, more complex networks only appear they are more fit.

    \section{Method and Results}
    \subsection{Data Preparation}
    First, a dataset of 3600 $\delta T_b(z)$ curves were generated using ARES. They were then normalized between 1 and 2 (not 0 and 1 as this causes divide by zero error) using the paramter/temperature bounds via the map $\text{var}\rightarrow \frac{\text{var} - \text{min}}{\text{max} - \text{min}} + 1$. In order to obtain the parameter ranges for ARES's inputs, they were individually varied to see which ranges produced a $z_\text{reionization}\in \left(0.046, 0.078\right)$, this is the Planck survey range\cite{plank}.
    
    \begin{table}[!ht]
        \begin{tabular}{|l|l|l|l|}
        \hline
        Parameter          & Minimum                                           & Maximum                                           & Distribution \\ \hline
        $T_\text{min}$     & $1\times 10^3$                                    & $2.5\times 10^5$                                  & Logarithmic  \\ \hline
        $f_\text{esc}$     & 0.108                                             & 0.358                                             & Linear       \\ \hline
        $c_X$              & $5\times 10^{38}$ & $5\times 10^{40}$ & Logarithmic  \\ \hline
        $\gamma_\text{lo}$ & 0.378                                             & 0.6605                                            & Linear       \\ \hline
        $f_{*,p}$          & 0.0271                                            & 0.0892                                            & Linear  \\ \hline
        \end{tabular}
        \caption{List of the 5 parameters used with the minimum and maximum value of each used. 40 evenly spaced redshift points from 5 to 30 were selected for each curve made by ARES. The ARES parameters to produce these curves were selected by a uniform or log-uniform distribution (last column).}
        \label{params}
    \end{table}
    The parameters were chosen in a way such that there are not degeneracies in the curves produced. This is important for ARES to be invertible. $\gamma_\text{lo}$ and $f_*,p$ are chosen because they influence $f_*(M_h) = \frac{2f_*,p}{\left(\frac{M_h}{M_p}\right)^{\gamma_{lo}} + \left(\frac{M_h}{M_p}\right)^{\gamma_{hi}}}$ \cite{m2018}.
    This is the the fraction of the baryonic matter that
    collapse into star as a function of the maximum and minimum masses that a star can take. Only 2 of the 3 variables defining $f_*$ can be independantly specified to explore the full functional range of $f_*$. The minimal virial temperature $T_\text{min}$, is easier to specify than the minimal virial mass for halo collapse $m_\text{min}$, since it is redshift independant and has an invertible relation with $m_\text{min} = 1.05\times 10^7\left(\frac{21T_\text{min}}{10^4K(1+z)}\right)\left(\frac{0.3}{\Omega_m}\right)^\frac{1}{2}\frac{0.7}{h}$ \cite{ARES}.
    This also does not influence the previous cosmological parameters, maintaining the lack of degeneracy. $f_\text{esc}$ and $c_X$ are the fraction of light that escape haloes and $c_X$ is the amount of light that is emited per star formation rate. This, along with many more linear factors appear in repeated multiplied combinations in several places in the code base but not all of them are needed to completely span the overall set of factors they produce, mathematically speaking. For the purpose of brevity, it suffices to say that only two need to be specified, and these two were chosen for their easy physical interpretation to avoid degeneracy.

    \subsection{Testing Methods}    
    The dataset was split into training and testing sets (90\%-10\% split) and the error on the testing set was evalated. This is done because these programs exactly mimic the result of the data they are trained on, so errors on training sets is not insightful. Therefore, they are tested on inputs that have never been seen before, as in their use cases. The accuracy metric of choice here is fractional difference ($\varepsilon$):
    \begin{equation}
        \varepsilon\equiv\frac{2(\|X_r - X_e\|)}{\|X_r + X_e\|}
    \end{equation}
    $\varepsilon$ is effectively a difference over sum of the reported ($X_r$) and expected ( $X_e$) values. It is aimed at characterizing the percent difference of either value from their mean.

    The RBFI implementation used was that of SciPy, a quintic ($y = x^5$) kernel was used with the default settings\cite{rbfi-sp}. Sci-kit Learn's MLPRegressor was used, 3 hidden layer of 100 nodes were used and LBFGS was along with default setting. LBFGS performs better for smaller datasets (by small, it is meant less than $10^4$) \cite{lbfgs}\cite{mlp}.  The \verb|neat-python| implementation was used with default settings with a fitness function of $1 - \bar{\varepsilon}$ \cite{neat-py}. This allows for a ``great is better'' metric.

    \begin{table}[!ht]
        \begin{tabular}{|l|l|l|l|}
        \hline
        Task           & $\varepsilon$ (\%) & Training (s)      & Evaluation (s)        \\ \hline
        RBFI Emulation & 2.107              & 0.27              & $3.15\times 10^{-5}$  \\ \hline
        RBFI Inversion & 3.043              & 0.76              & $1.45 \times 10^{-4}$ \\ \hline
        MLP Emulation  & 3.718              & 75.15             & $3.05 \times 10^{-6}$ \\ \hline
        MLP Inversion  & 4.745              & 360.95            & $2.34 \times 10^{-6}$ \\ \hline
        NEAT Emulation & 194.4              & 14766.53          & $6.0\times 10^{-6}$   \\ \hline
        NEAT Inversion & 183.1              & 13800.73          & $1.52 \times 10^{-7}$ \\ \hline
        \end{tabular}
        \caption{Error on the testing set ($\varepsilon$), Time taken to create the function (Training time), and average time taken to be evaluated for any single input. Note that for the given hardware (Intel\textsuperscript\textregistered i9-10900 CPU), ARES takes no less than 5 seconds per input for evaluation.}
        \label{results}
    \end{table}
        

    \section{Discussion}
    RBFI was most accurate with 2.107\% and 3.043\% errors in emulation and inversion. It also trained fastest, taking $0.27$s and $0.76$s respectively. While MLP was slightly less accurate than RBFI and took up to $\sim$100 times longer to train, it evaluated up to $\sim$100 times faster.
    
    While the original curve (in Figure \ref{plots}, $z\approx 5$) simply flattens out during the X-ray heating/EoR transistion period (representing actual $\delta T_b\rightarrow 0$), in reality $\delta T_b$ actually becomes slightly positive in this time. Due to the low redshift resolution and some parameter combinations, many original curves do not do this. However, the MLP seems to retain information from the curves that do, recreating this ``peak'' in several curves. Qualitatively, it would seem that MLP is better able to capture physical trends, even though it is less accurate quantitatively. Lastly, NEAT does not show any evidence of fitting the curve well. Organisms evolve to be better than the competition, but need not go further to survive. It would appear that it is not likely enough that large, impactful mutations occur often enough to make sufficiently fit organisms.
    
    \begin{figure}[!ht]
        \begin{center}
            \includegraphics[width = 0.8 \linewidth]{../data/plots.png}
            \caption{A sample plot of the emulators. RBFI and MLPs seem to be good fits but NEAT does not.}
            \label{plots}
        \end{center}
    \end{figure}

    \section{Conclusions}
    RBFI and MLPs seem to provide suitable models for ARES $\delta T_b(z)$ curves their inverses. The NEAT algorithm used is unable to attain the complexity required to accomplish these tasks. Therefore, future work would involve only the use of RBFI and MLP. In the future, different parameters of these two programs can be varied to maximize accuracy. Another possible expansion is generating ARES curves to emphasize the more critical times in the universe's history. Currently a uniformly distributed sample of redshifts is used, but a higher density around transtions between epochs might provide emulations with more fidelity to the expected behaviour.

    \setstretch{0.1}
    \small
    \begin{thebibliography}{16}
        \bibitem{ARES} Mirocha, J. et al. \href{https://github.com/mirochaj/ares}{ARES Code Base}
        \bibitem{f2006} Furlanetto, 2006. \href{https://iopscience.iop.org/article/10.1086/509597/pdf}{Simulations and Analytic Calculations of Bubble Growth during Hydrogen Reionization}
        \bibitem{Zygelman} Zygelman, B. 2005. \href{https://iopscience.iop.org/article/10.1086/427682/pdf}{Hyperfine Level-changing Collisions of Hydrogen Atoms and Tomography of the Dark Age Universe}
        \bibitem{m2012} Mirocha, J. et al. 2012. \href{https://arxiv.org/pdf/1204.1944.pdf}{Optimized Multi-frequency Spectra for Applications in Radiative Feedback and Cosmological Reionization}
        \bibitem{fs2010} Furlanetto, Stoever. 2010. \href{https://watermark.silverchair.com/mnras0404-1869.pdf?token=AQECAHi208BE49Ooan9kkhW_Ercy7Dm3ZL_9Cf3qfKAc485ysgAAAtswggLXBgkqhkiG9w0BBwagggLIMIICxAIBADCCAr0GCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM9_dGCR5liPYkPrO5AgEQgIICjt0StWzWIwSq9wtyeH2tiKsx9uY-B7hFBhDEJ-6kFnA7reY33ZmnXqYDaHuQUuWpf5T6ocHm1G-yFTpqT523m4JRNkc_o2GK_uTUTRZsmVX6R2TWMczfjnoYnMrreGKknFr2Jesk-6qqGhrTwIeRurKJEmBixe_yrJ-Uxw0BHZ8gi1ype0P8LgK6-jbEGm0G3AObq81Z8Av7oefi4IoHOjIo9LqYkBkeuUnKZzPa9_RI0CSOP28tQMXaoCnx5v-cEs_qHAh2VETasbmocs_7pja-2a0lDSJcjFgyGVhmf88NYppiovq-XrltrjzMCYrBqvdR4WYNWQzMavumM2zRhvcl0akJYJUvUwUT_YpDHSE59gjmEvUToyW5H-TWIGqIhcONbeIH0okX5RxV7yzhtdsnIusPB800fQ-zcSomiiwJs89AbqMtLaqXM-CJrtn_1w5ewECGxSuxqaE6y4C-LFOBW6ztOk87Jjwe9yFvGSTa6CsRJBffS_VnFMIHrJLpMLj4cVqagqRMvliU9B47f19x96jorTdvpFQjSqF0TT-P5pdLJInTUnT4Ud05BCONOgaV1-SRSG5HkP1l9kZ6BsUmYTXklEMu8E-4h4GFycuI8tM0UAUhUZDXPHmiGET3GVmNv_H-RV0YMqD33de02G2gZ0AIRsGtTsfSnqzPj-J8vgv9jYySd7FpV534SpO8myUWYQgBzzg-8LzIavYUsOcuAkPCv0XU80lZiwWYw1rQ-630XIM2ppHd78tFgBwUIoymEsMz4QNyGBGEo230cGB_ribItlgmWzD6qt0K1B1XSBwrvG6d3dWz4AzzRbV1E72kUD0BZ3mmLKyvkvqSzqYRS62WGCV_EW3WXNWNkA}{Secondary ionization and heating by fast electrons}
        \bibitem{hmf}Murray, et at. 2013. \href{https://arxiv.org/pdf/1306.6721.pdf}{HMFcalc: An Online Tool for Calculating Dark Matter Halo Mass Functions}
        \bibitem{m1956} J. C. Mairhuber. 1956. \href{https://www.jstor.org/stable/2033359}{On Haar's Theorem Concerning Chebychev Approximation Problems Having Unique Solutions}
        \bibitem{nn} Hastie, T. et al. 2017. \href{https://link.springer.com/content/pdf/10.1007/978-0-387-84858-7.pdf}{The Elements of Statistical Learning: Data Mining, Inference, and Prediction}
        \bibitem{NEAT} Stanley, K, O. Miikkulainen, R. 2002. \href{https://nn.cs.utexas.edu/downloads/papers/stanley.ec02.pdf}{Evolving Neural Networks through
        Augmenting Topologies}
        \bibitem{plank} Lawrence, C. R. et al. 2016. \href{https://www.aanda.org/articles/aa/pdf/2016/10/aa27101-15.pdf}{Planck 2015 results. I. Overview of products and results}
        \bibitem{m2018} Mirocha, J. et al. 2018. \href{https://arxiv.org/pdf/1710.02530.pdf}{Unique Signatures of Population III Stars in the Global
        21-cm Signal}
        \bibitem{rbfi-sp} \textit{scipy.org}. \href{https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.RBFInterpolator.html}{RBFInterpolator}
        \bibitem{lbfgs} Fletcher, R. 1970. \href{https://academic.oup.com/comjnl/article/13/3/317/345520?login=false}{A new approach to variable metric algorithms}
        \bibitem{mlp} \textit{scikit-learn.org}. \href{https://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPRegressor.html}{MLPRegressor}
        \bibitem{neat-py} \textit{Code Reclaimers}. \href{https://github.com/CodeReclaimers/neat-python}{NEAT-Python}
    \end{thebibliography}



\end{document}