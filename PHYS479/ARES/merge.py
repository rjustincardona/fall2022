from lib import *

p1 = np.loadtxt("data/ares_params_0.gz")
t1 = np.loadtxt("data/ares_dTb_0.gz")

p2 = np.loadtxt("data/ares_params_2.gz")
t2 = np.loadtxt("data/ares_dTb_2.gz")

p = np.vstack([p1, p2])
t = np.vstack([t1, t2])

np.savetxt("data/ares_params.gz", p)
np.savetxt("data/ares_dTb.gz", t)
