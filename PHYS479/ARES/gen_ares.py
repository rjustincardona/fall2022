from lib import  *


t = time.time()
gen_dataset(1, filename = ["data/ares_params_0.gz", "data/ares_dTb_0.gz"], new=True)
gen_dataset(2000, filename = ["data/ares_params_0.gz", "data/ares_dTb_0.gz"])
print(time.time() - t)

t = time.time()
gen_dataset(1, filename = ["data/ares_params_1.gz", "data/ares_dTb_1.gz"], new=True)
gen_dataset(2000, filename = ["data/ares_params_1.gz", "data/ares_dTb_1.gz"])
print(time.time() - t)