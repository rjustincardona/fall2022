from lib import  *


t = time.time()
gen_dataset(1, filename = ["data/ares_params_2.gz", "data/ares_dTb_2.gz"], new=True)
gen_dataset(2000, filename = ["data/ares_params_2.gz", "data/ares_dTb_2.gz"])
print(time.time() - t)

t = time.time()
gen_dataset(1, filename = ["data/ares_params_3.gz", "data/ares_dTb_3.gz"], new=True)
gen_dataset(2000, filename = ["data/ares_params_3.gz", "data/ares_dTb_3.gz"])
print(time.time() - t)