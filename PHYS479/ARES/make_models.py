from lib import *
import time
import matplotlib.pyplot as plt

# Preparing the data
p, t, P, T = preprocess("data/ares_params.gz", "data/ares_dTb.gz", mix=False)
p += 1
P += 1
t += 1
T += 1

# end = 999
# N = 100

# t = t.T[:end: int(end / N)].T
# T = T.T[:end: int(end / N)].T
# redshifts = redshifts[:end: int(end / N)]

# Making the RBFI
rbfi_t_name = "models/rbfi_t.p"
rbfi_t_time = make_rbfi(p, t, name=rbfi_t_name)
with open(rbfi_t_name, "rb") as f:
    rbfi_t = pickle.load(f)

# rbfi_p_name = "models/rbfi_p.p"
# rbfi_p_time = make_rbfi(t, p, name=rbfi_p_name)
# with open(rbfi_p_name, "rb") as f:
#     rbfi_p = pickle.load(f)

# Making the MLP
mlp_t_name = "models/mlp_t.p"
mlp_t_time = make_mlp(p, t, name=mlp_t_name)
with open(mlp_t_name, "rb") as f:
    mlp_t = pickle.load(f)

mlp_p_name = "models/mlp_p.p"
mlp_p_time = make_mlp(t, p, name=mlp_p_name)
with open(mlp_p_name, "rb") as f:
    mlp_p = pickle.load(f)

# Fitness Metrics

N = 0
plt.plot(redshifts, rbfi_t([P[N]])[0], label = "RBFI")
plt.plot(redshifts, mlp_t.predict([P[N]])[0], label = "MLP")
plt.plot(redshifts, T[N], label = "Original Curve")
plt.ylabel(r"$\delta T_b$ unitless and normalized")
plt.xlabel(r"$z$ redshift")
plt.legend()
plt.show()
plt.close()


print("Task\t\t Training Error\t\t\t Testing Error\t\t Training Time")
ti = time.time()

print("RBFI Emulation\t", err(rbfi_t(p), t), '\t', err(rbfi_t(P), T), '\t', rbfi_t_time)
print((time.time() - ti)/ T.shape[0])
# ti = time.time()
# print("RBFI Inversion\t", err(rbfi_p(t), p), '\t', err(rbfi_p(T), P), '\t', rbfi_p_time)
# print((time.time() - ti)/ t.shape[0])
# ti = time.time()

print("MLP Emulation\t", err(mlp_t.predict(p), t), '\t\t', err(mlp_t.predict(P), T), '\t', mlp_t_time)
print((time.time() - ti)/ t.shape[0])
ti = time.time()
print("MLP Inversion\t", err(mlp_p.predict(t), p), '\t\t', err(mlp_p.predict(T), P), '\t', mlp_p_time)
print((time.time() - ti)/ t.shape[0])
ti = time.time()


