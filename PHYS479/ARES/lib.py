import numpy as np
import pickle
from scipy.interpolate import RBFInterpolator
from sklearn.neural_network import MLPRegressor
from sklearn.utils import shuffle
import time
import ares
from globalemu.downloads import download
from globalemu.eval import evaluate
import matplotlib.pyplot as plt
from scipy.interpolate import CubicSpline

# The ranges of the input variables
input_bounds = [
	["pop_Tmin_0_", 1E3, 1.25E5, "log"],
	["pop_fesc_0_", 0.108, 0.358, "lin"],
	["pop_rad_yield_1_", 5E38, 5E40, "log"], #cX
	["pq_func_par2[0]_0_", 0.378, 0.6605, "lin"], #gamma_lo
	["pq_func_par0[0]_0_", 0.0271, 0.0892, "lin"], #fstar,p
	]


# input_bounds = [
# 	    ["fstar", 1E-3, 5E-1, "log"],
#         ["pop_Tmin", 1E3, 1E5, "log"],
#         ["fX", 0, 1E3, "lin"],
#         ["Nion", 3e3, 5e3, "log"],
#         ["fesc", 0, 1, "lin"]
# 	]

# Functions for fitting data using various techniques
def err(x, y, rmse = True):
    '''
    Calculated relative error between two datasets
    Parameters
    ----------
    x: array like
        array 1
    y: array like
        array 2

    Returns
    -------
    err: float
        the error between the two sets
    '''
    if rmse:
        return np.sqrt(np.mean((x - y) ** 2))
    return np.mean(np.abs(x - y) / (np.abs((x + y) / 2)))
    # return np.mean(np.abs(x - y) / (np.abs((x + y) / 2)))
    return np.sqrt(np.mean((np.abs(x - y) / x) ** 2))


def normalize(arr, output = False):
    '''
    Normalizes a dataset to the range in input_bounds or [0, 1] for outputs

    Parameters
    ----------
    arr: arr like
        data to be normalized
    Returns
    -------
    arr:
        normalized data
    '''
    if output == False:
        for i in range(arr.shape[1]):
                if input_bounds[i][-1] == "lin":
                    arr[:, i] = (arr[:, i] - input_bounds[i][1]) / (input_bounds[i][2] - input_bounds[i][1])
                elif input_bounds[i][-1] == "log":
                    arr[:, i] = (np.log10(arr[:, i] / input_bounds[i][1])) / (np.log10(input_bounds[i][2] / input_bounds[i][1]))
        
        return arr
    else:
        return (arr - np.min(arr)) / (np.max(arr) - np.min(arr))


def preprocess(y, d, ratio = 0.95, sample = 1, mix = True):
    '''
    Unpacks a data set into training/testing partitions and normalizes them

    Parameters
    ----------
    y: string
        file name for input data
    d: string
        file name for output data

    Returns
    -------
    p, t, P, T: numpy arrays
        the training and testing sets
    '''
    # Opening the data
    y = np.loadtxt(y)
    d = np.loadtxt(d)

    y = y[:int(y.shape[0] * sample)]
    d = d[:int(d.shape[0] * sample)]
    if mix:
        y, d = shuffle(y, d)

    y = normalize(y)
    d = normalize(d, output = True)

    # splitting the data into traing and testing
    r = int(ratio * y.shape[0])
    y_train = y[:r]
    d_train = d[:r]
    y_test = y[r:]
    d_test = d[r:]

    # y_train = normalize(y_train)
    # d_train = normalize(d_train, output = True)
    # y_test = normalize(y_test)
    # d_test = normalize(d_test, output = True)

    return y_train, d_train, y_test, d_test


def make_rbfi(y, d, neighbors = None, kernel = 'quintic', epsilon = None, degree = None, name = "rbfi.p"):
    '''
    Fits an RBF to some data, RBFInterpolators optional arguments can also be supplied

    Paramters
    ---------
    y: array like
        The interpolation points.
    d: array like
        The interpolation points' values.
    name: string
        The name of the file to which the RBFI is saved

    Returns
    -------
    time_rbfi: float
        The time it took to complete the RBFI creation
    '''
    time_start = time.time()
    rbfi = RBFInterpolator(y, d, neighbors, kernel = kernel, epsilon = epsilon, degree = degree)
    time_rbfi = time.time() - time_start
    with open(name, "wb") as f:
        pickle.dump(rbfi,f)
    return time_rbfi


def make_mlp(y, d, layer=(100, 100, 100,), max=1000, t=1e-5, name = "mlp.p"):
    '''
    Fits an MLP to some data, some of MLPRegressor's optional arguments can also be supplied

    Paramters
    ---------
    y: array like
        The training inputs.
    d: array like
       The training outputs.
    name: string
        The name of the file to which the MLP is saved

    Returns
    -------
    time_mlp: float
        The time it took to complete the MLP creation
    '''
    if y.shape[0] <= 1e4:
        s = 'lbfgs'
    else:
        s = 'sgd'
    mlp = MLPRegressor(hidden_layer_sizes=layer, max_iter=max, solver=s, learning_rate='adaptive', verbose=10, tol=t, random_state=1, n_iter_no_change= max / 2)
    time_start = time.time()
    mlp.fit(y, d)
    time_mlp = time.time() - time_start
    with open(name, "wb") as f:
        pickle.dump(mlp,f)
    return time_mlp


# Functions for operations relating to cosmology applications
redshifts = np.linspace(5, 50, 1000)

# Create ares dataset
def make_curve(params):
    simu = ares.simulations.Global21cm(**params, verbose=False, progress_bar=False)
    simu.run()  
    z = simu.history['z'][::-1]
    dTb = simu.history['dTb'][::-1]

    sorted_idx = np.argsort(z,kind="stable")
    z = z[sorted_idx]
    dTb = dTb[sorted_idx]
    splinedTb = CubicSpline(z, dTb)
    return splinedTb(redshifts), z


# Select from parameter range
def sample_param():
    result = {}
    for i in range(len(input_bounds)):
        if input_bounds[i][3] == "lin":
            result[input_bounds[i][0]] = np.random.uniform(input_bounds[i][1], input_bounds[i][2])
        else:
            result[input_bounds[i][0]] = np.power(10, np.random.uniform(np.log10(input_bounds[i][1]), np.log10(input_bounds[i][2])))
    return result


# Generate N points and adds them to the dataset
def gen_dataset(N = 1, filename = ["data/ares_params.gz", "data/ares_dTb.gz"], new = False):
    for i in range(N):
        params = sample_param()
        vals = np.array(list(params.values()))
        dTb = make_curve(params)[0]
        
        if new:
            p = np.array(vals, dtype = float)
            t = dTb
            np.savetxt(filename[0], p)
            np.savetxt(filename[1], t)
        else:
            p = np.loadtxt(filename[0])
            t = np.loadtxt(filename[1])
            p = np.vstack([p, np.array(vals, dtype = float)])
            t = np.vstack([t, dTb])
            np.savetxt(filename[0], p)
            np.savetxt(filename[1], t)


# Use globalemu
def gemu(params):
    # download().model()
    predictor = evaluate(base_dir='T_release/') # Redshift-Temperature Network
    return predictor(params)
